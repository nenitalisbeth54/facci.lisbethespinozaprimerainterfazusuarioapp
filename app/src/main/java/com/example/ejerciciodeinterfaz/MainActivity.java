package com.example.ejerciciodeinterfaz;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {

    Button boton_c,boton_f;
    EditText in_celcius, in_fahrent;
    TextView out_c,out_f;
    DecimalFormat df = new DecimalFormat("#.0");

    double in_ce = 0;
    double in_fa = 0;
    double out_ce = 0;
    double out_fe = 0;

    private String TAG = "MainAtivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        boton_c=findViewById(R.id.id_button_c);
        boton_c.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                in_celcius = (EditText)findViewById(R.id.id_c_data);
                out_c = (TextView)findViewById(R.id.id_c_out);
                in_ce = Double.parseDouble(in_celcius.getText().toString());
                out_fe = (in_ce*1.8)+32;
                out_c.setText(df.format(out_fe) + "°F");
            }
        });

        boton_f=findViewById(R.id.id_button_f);
        boton_f.setOnClickListener(new View.OnClickListener()

        {

            @Override
            public void onClick (View v){
                in_fahrent = (EditText) findViewById((R.id.id_f_data));
                out_f = (TextView) findViewById(R.id.id_f_out);
                in_fa = Double.parseDouble(in_fahrent.getText().toString());
                out_ce = (in_fa - 32) * 5 / 9;
                out_f.setText(df.format(out_ce) + "°C");
            }
        });

        Log.i(TAG, "Gabriela Espinoza");
    }
}